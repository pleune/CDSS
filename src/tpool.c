#include "cdss/tpool.h"

#ifndef __STDC_NO_THREADS__

#include <stdlib.h>
#include <string.h>
#include <threads.h>
#include <assert.h>

struct work_queue {
    struct work_queue *next;

    tpool_work_t work;
    void *arg;

    int used;
};

struct queue_pool {
    struct work_queue slot[TPOOL_MAX_WORK_QUEUE];

    struct queue_pool *next;
};

struct threadpool {
    unsigned int num_threads;
    int stop;
    int paused;

    thrd_t threads[TPOOL_MAX_THREADS];
    struct work_queue *queue;

    //protects queue
    mtx_t mut;

    //used to notify worker() of new work
    cnd_t cond;
    mtx_t cond_mut;

    //used to notify tpool_flush() of work being finished
    cnd_t flush;
    mtx_t flush_mut;
};

static struct queue_pool pool = {{{0, 0, 0, 0}}, 0};

static int
worker(void *arg)
{
    tpool_t *data = arg;

    while(data->stop == 0)
    {
        if(data->queue == 0)
            while((data->paused || !data->queue) && !data->stop)
            {
                // wait for work
                mtx_lock(&data->cond_mut);
                cnd_wait(&data->cond, &data->cond_mut);
                mtx_unlock(&data->cond_mut);
            }

        if(data->stop)
            break;

        tpool_work_t func = 0;
        void *arg;

        mtx_lock(&data->mut);
        if(data->queue != 0)
        {
            func = data->queue->work;
            arg = data->queue->arg;
            data->queue->used = 0;
            data->queue = data->queue->next;
        }

        mtx_unlock(&data->mut);

        if(func != 0)
            func(arg);

        // if flush is waiting, wake it
        mtx_lock(&data->flush_mut);
        cnd_broadcast(&data->flush);
        mtx_unlock(&data->flush_mut);
    }

    return 0;
}

tpool_t *
tpool_create(unsigned int threads)
{
    assert(threads > 0 && threads <= 256);

    tpool_t *ret = malloc(sizeof(struct threadpool));

    if(ret == 0)
        return 0;

    ret->stop = 0;
    ret->queue = 0;
    ret->paused = 0;
    ret->num_threads = threads;

    int status_mut = mtx_init(&ret->mut, mtx_plain);
    if(status_mut != thrd_success) goto tpool_create_fail;

    int status_cond = cnd_init(&ret->cond);
    if(status_cond != thrd_success) goto tpool_create_fail;

    int status_cond_mut = mtx_init(&ret->cond_mut, mtx_plain);
    if(status_cond_mut != thrd_success) goto tpool_create_fail;

    int status_flush = cnd_init(&ret->flush);
    if(status_flush != thrd_success) goto tpool_create_fail;

    int status_flush_mut = mtx_init(&ret->flush_mut, mtx_plain);
    if(status_flush_mut != thrd_success) goto tpool_create_fail;

    size_t i;
    for(i=0; i<threads; i++)
    {
        int status = thrd_create(&ret->threads[i], &worker, ret);
        if(status != thrd_success)
        {
            ret->num_threads = i; // trick destroy into only destroying up to this point
            tpool_destroy(ret);
            return 0;
        }
    }

    return ret;

tpool_create_fail:

    if (status_mut != thrd_success) mtx_destroy(&ret->mut);
    if (status_cond != thrd_success) cnd_destroy(&ret->cond);
    if (status_cond_mut != thrd_success) mtx_destroy(&ret->cond_mut);
    if (status_flush != thrd_success) cnd_destroy(&ret->flush);
    if (status_flush_mut != thrd_success) mtx_destroy(&ret->flush_mut);
    free(ret);

    return 0;
}

void
tpool_destroy(tpool_t *t)
{
    t->stop = 1;

    //wake threads to let them stop
    mtx_lock(&t->cond_mut);
    cnd_broadcast(&t->cond);
    mtx_unlock(&t->cond_mut);

    size_t i;
    for(i=0; i<t->num_threads; i++)
        thrd_join(t->threads[i], 0);

    mtx_destroy(&t->mut);
    cnd_destroy(&t->cond);
    mtx_destroy(&t->cond_mut);
    cnd_destroy(&t->flush);
    mtx_destroy(&t->flush_mut);
    free(t);
}

void
tpool_add(tpool_t *t, tpool_work_t work, void *arg, int front)
{
    //Find queue obj
    struct queue_pool *p_ptr = &pool;
    size_t i = 0;

    while(p_ptr->slot[i].used != 0)
    {
        i++;

        if(i == 4096)
        {
            i = 0;
            if(p_ptr->next == 0)
                p_ptr->next = calloc(sizeof(struct queue_pool), 1);

            p_ptr = p_ptr->next;
        }
    }

    struct work_queue *queue_node = &(p_ptr->slot[i]);

    //Lock mutex while working with queue
    mtx_lock(&t->mut);

    if(front)
    {
        queue_node->next = t->queue;
        t->queue = queue_node;
    } else {
        struct work_queue **work_queue_place = &(t->queue);
        while(*work_queue_place != 0)
            work_queue_place = &((*work_queue_place)->next);
        *work_queue_place = queue_node;

        queue_node->next = 0;
    }

    queue_node->work = work;
    queue_node->arg = arg;
    queue_node->used = 1;

    mtx_unlock(&t->mut);

    mtx_lock(&t->cond_mut);
    cnd_broadcast(&t->cond);
    mtx_unlock(&t->cond_mut);
}

void
tpool_pause(tpool_t *t)
{
    t->paused = 1;
}

void
tpool_resume(tpool_t *t)
{
    t->paused = 0;

    //notify threads of resume
    mtx_lock(&t->cond_mut);
    cnd_broadcast(&t->cond);
    mtx_unlock(&t->cond_mut);
}

void
tpool_flush(tpool_t *t)
{
    while(t->queue != 0)//single read needs no mutex lock.
    {
        //wait for update
        mtx_lock(&t->flush_mut);
        cnd_wait(&t->flush, &t->flush_mut);
        mtx_unlock(&t->flush_mut);
    }
}

#endif