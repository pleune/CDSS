#include "cdss/hash.h"

uintptr_t
hash_fib(uintptr_t i)
{
#define P 1.61803398874989484820458683436563811772030917980576286213544862270526
    const static uintptr_t s = UINTPTR_MAX / P;
    return s * i;
#undef P
}

// 64-bit FNV1-a
// http://www.isthe.com/chongo/tech/comp/fnv/ is a good link
uintptr_t
hash_faststr(const unsigned char *s)
{
#if UINTPTR_MAX == 18446744073709551615U
#define FNV_prime 1099511628211U
#define offset_basis 14695981039346656037U
#elif UINTPTR_MAX == 4294967296U
#define FNV_prime 16777619U
#define offset_basis 2166136261U
#else
#error hash_faststr non-supported UINTPTR_MAX size
#endif

    unsigned char c;
    uintptr_t hash = offset_basis;
    while ((c = *s++))
    {
        hash ^= c;
        hash *= FNV_prime;
    }

#undef FNV_prime
#undef offset_basis

    return hash;
}
