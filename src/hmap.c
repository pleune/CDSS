#include "cdss/hmap.h"

#include "cdss/hash.h"

// data stored in blocks to make better use of memory with alignment
// requirements. The metadata is only 1 byte, so store many of them together.
struct block {
    uint8_t metadata[16];
    const void *key[16];
    const void *val[16];
};

struct hmap {
    hmap_hash_t hashf;
    hmap_cmp_t cmpf;
    unsigned int index_bits, index_unused_bits;
    size_t num_indexes, num_indexes_minus_one;
    size_t num_keys;
    double max_load_factor;
    struct block *data;
};

static size_t jmp_distances[127] = {0};

static inline size_t
num_jmp_distances()
{
    return sizeof(jmp_distances) / sizeof(*jmp_distances);
}

static inline size_t
get_triangle_number(const size_t n)
{
    return n * (n + 1) / 2;
}

static void
init_jmp_distances()
{
    for (int i = 0; i < 16; i++) jmp_distances[i] = i;

    for (int i = 16; i < 82; i++)
        jmp_distances[i] = get_triangle_number(i - 10);

    for (int i = 82, n = 86; i < 126; i++, n *= 1.5)
        jmp_distances[i] = get_triangle_number(n);
}

const static uint8_t meta_empty    = 0b11111111; // node is not used
const static uint8_t meta_bits_jmp = 0b01111111; // jmp table index
const static uint8_t meta_child    = 0b10000000; // list decendent
const static uint8_t meta_root     = 0b00000000; // first list element

static inline int
meta_ischild(const uint8_t metadata)
{
    return ((metadata & meta_child) == meta_child) && metadata != meta_empty;
}

static inline int
meta_isroot(const uint8_t metadata)
{
    return (metadata & meta_child) == meta_root;
}

static inline int
meta_isempty(const uint8_t metadata)
{
    return metadata == meta_empty;
}

static inline size_t
meta_jmp(const uint8_t metadata)
{
    return jmp_distances[metadata & meta_bits_jmp];
}

size_t
hash_to_index(uintptr_t hash, unsigned int unused_bits)
{
    return hash_fib(hash) >> unused_bits;
}

static inline size_t
next_pow_two(size_t i)
{
    --i;
    i |= i >> 1;
    i |= i >> 2;
    i |= i >> 4;
    i |= i >> 8;
    i |= i >> 16;
    i |= i >> 32;
    return i + 1;
}

static inline unsigned int
bits_for_pow_two(size_t i)
{
    unsigned int ret;
    for (ret = 0; !(i & 1); ret++, i >>= 1)
        ;
    return ret;
}

static void
grow(struct hmap *m)
{
    struct block *old_data       = m->data;
    const size_t old_num_indexes = m->num_indexes;
    const size_t new_num_indexes = old_num_indexes * 2;

    m->num_indexes           = new_num_indexes;
    m->num_indexes_minus_one = new_num_indexes - 1;
    m->index_bits++;
    m->index_unused_bits--;
    m->num_keys = 0;

    struct block *new_data =
        (struct block *)malloc(sizeof(struct block) * (new_num_indexes / 16));
    m->data = new_data;
    // TODO: handle malloc fail

    const size_t old_num_blocks = old_num_indexes / 16;
    const size_t new_num_blocks = new_num_indexes / 16;

    for (size_t i = 0; i < new_num_blocks; i++)
        for (uint8_t j = 0; j < 16; j++) new_data[i].metadata[j] = meta_empty;

    for (size_t i = 0; i < old_num_blocks; i++)
        for (uint8_t j = 0; j < 16; j++)
            if (!meta_isempty(old_data[i].metadata[j]))
                hmap_insert(m, old_data[i].key[j], old_data[i].val[j]);

    free(old_data);
}

struct hmap *
hmap_create(size_t size, hmap_hash_t hashf, hmap_cmp_t cmpf)
{
    if (jmp_distances[1] == 0) // table not initalized
        init_jmp_distances();

    if (size < 16)
        size = 16;
    else
        size = next_pow_two(size);

    struct hmap *ret = (struct hmap *)malloc(sizeof(struct hmap));
    if (!ret) return 0;

    ret->data = (struct block *)malloc(sizeof(struct block) * (size / 16));
    if (!ret->data)
    {
        free(ret);
        return 0;
    }

    ret->hashf                 = hashf;
    ret->cmpf                  = cmpf;
    ret->num_indexes           = size;
    ret->num_indexes_minus_one = size - 1;
    ret->index_bits            = bits_for_pow_two(ret->num_indexes);
    ret->index_unused_bits     = 64 - ret->index_bits;
    ret->num_keys              = 0;
    ret->max_load_factor       = 0.9;

    for (size_t i = 0; i < size / 16; i++)
        for (uint8_t j = 0; j < 16; j++) ret->data[i].metadata[j] = meta_empty;

    return ret;
}

void
hmap_destroy(struct hmap *m)
{
    free(m->data);
    free(m);
}

void
hmap_set_max_load(struct hmap *m, double load)
{
    m->max_load_factor = load;
    // TODO: grow if needed
}

size_t
hmap_num_keys(const struct hmap *m)
{
    return m->num_keys;
}

size_t
hmap_find(const struct hmap *m, const void *key)
{
    uintptr_t index;
    const hmap_hash_t hashf = m->hashf;
    if (hashf)
        index = hashf(key);
    else
        index = (uintptr_t)key;
    index           = hash_to_index(index, m->index_unused_bits);
    hmap_cmp_t cmpf = m->cmpf;

    int first = 1;
    for (;;)
    {
        int index_into_block      = index % 16;
        const struct block *block = &m->data[index / 16];
        uint8_t metadata          = block->metadata[index_into_block];

        if (first)
        {
            if (meta_ischild(metadata) || meta_isempty(metadata))
                return -1; // key not in map
            first = 0;
        }

        if (!cmpf)
        {
            if (key == block->key[index_into_block]) return index;
        }
        else
        {
            if (!cmpf(key, block->key[index_into_block])) return index;
        }

        size_t jmp = meta_jmp(metadata);
        if (jmp == 0) return -1; // end of list. no more children to search
        index = (index + jmp) & m->num_indexes_minus_one;
    }
}

const void *
hmap_at(const struct hmap *m, size_t index)
{
    return m->data[index / 16].val[index % 16];
}

static uint8_t
find_free_jmp_index(struct block *data,
                    size_t index,
                    size_t num_indexes_minus_one)
{
    for (uint8_t i = 1; i < num_jmp_distances(); i++)
    {
        size_t new_index = (index + jmp_distances[i]) & num_indexes_minus_one;
        struct block *block = &data[new_index / 16];
        if (meta_isempty(block->metadata[new_index % 16])) return i;
    }

    return 0;
}

int
hmap_insert(struct hmap *m, const void *key, const void *val)
{
    if (m->num_keys + 1 > m->num_indexes * m->max_load_factor) grow(m);

    struct block *const data = m->data;
    uintptr_t index;
    const hmap_hash_t hashf = m->hashf;
    if (hashf)
        index = hashf(key);
    else
        index = (uintptr_t)key;
    index                     = hash_to_index(index, m->index_unused_bits);
    struct block *const block = &data[index / 16];
    const int8_t block_index  = index % 16;
    const uint8_t metadata    = block->metadata[block_index];
    const size_t num_indexes_minus_one = m->num_indexes_minus_one;

    if (meta_isroot(metadata))
    {
        size_t working_index        = index;
        struct block *working_block = &data[working_index / 16];
        int working_block_index     = working_index % 16;

        hmap_cmp_t cmpf = m->cmpf;
        for (;;)
        {
            if (!cmpf)
            {
                if (working_block->key[working_block_index] == key) return 0;
            }
            else
            {
                if (!cmpf(working_block->key[working_block_index], key))
                    return 0;
            }

            size_t jmp = meta_jmp(working_block->metadata[working_block_index]);
            if (jmp == 0) break;
            working_index       = (working_index + jmp) & num_indexes_minus_one;
            working_block       = &data[working_index / 16];
            working_block_index = working_index % 16;
        }

        size_t jmp_index =
            find_free_jmp_index(data, working_index, num_indexes_minus_one);

        if (!jmp_index)
        {
            grow(m);
            return hmap_insert(m, key, val);
        }

        working_block->metadata[working_block_index] |= jmp_index;
        working_index =
            (working_index + jmp_distances[jmp_index]) & num_indexes_minus_one;
        working_block       = &data[working_index / 16];
        working_block_index = working_index % 16;
        working_block->metadata[working_block_index] = meta_child;
        working_block->key[working_block_index]      = key;
        working_block->val[working_block_index]      = val;
        m->num_keys++;

        return 1;
    }
    else
    {
        if (meta_ischild(metadata))
        { // push index out
            const void *existing_key = block->key[block_index];
            uintptr_t parent         = hash_to_index(hashf ? hashf(existing_key)
                                                   : (uintptr_t)existing_key,
                                             m->index_unused_bits);
            struct block *parent_block;
            uint8_t parent_metadata;
            int parent_block_index;
            for (;;)
            {
                parent_block       = &data[parent / 16];
                parent_block_index = parent % 16;
                parent_metadata    = parent_block->metadata[parent_block_index];

                size_t jmp_distance = meta_jmp(parent_metadata);
                size_t next_index =
                    (parent + jmp_distance) & num_indexes_minus_one;
                if (next_index == index) break;
                parent = next_index;
            }

            size_t working_index        = index;
            struct block *working_block = &data[working_index / 16];
            int working_block_index     = working_index % 16;

            int first = 1;
            for (;;)
            {
                size_t parent_next_jmp =
                    find_free_jmp_index(data, parent, m->num_indexes_minus_one);
                if (!parent_next_jmp)
                {
                    grow(m);
                    return hmap_insert(m, key, val);
                }

                parent_block->metadata[parent_block_index] =
                    (parent_metadata & meta_child)
                    | parent_next_jmp; // rewrite pointer
                parent = (parent + jmp_distances[parent_next_jmp])
                         & num_indexes_minus_one;
                parent_block                               = &data[parent / 16];
                parent_block_index                         = parent % 16;
                parent_block->metadata[parent_block_index] = meta_child;
                parent_block->key[parent_block_index] =
                    working_block->key[working_block_index];
                parent_block->val[parent_block_index] =
                    working_block->val[working_block_index];

                uint8_t working_metadata =
                    working_block->metadata[working_block_index];
                if (first)
                {
                    block->metadata[block_index] = meta_root;
                    first                        = 0;
                }
                else
                {
                    working_block->metadata[working_block_index] = meta_empty;
                }
                size_t jmp = meta_jmp(working_metadata);
                if (jmp == 0) break;
                working_index = (working_index + jmp) & num_indexes_minus_one;
                working_block = &data[working_index / 16];
                working_block_index = working_index % 16;
            }
        }

        block->metadata[block_index] = meta_root;
        block->key[block_index]      = key;
        block->val[block_index]      = val;
        m->num_keys++;

        return 1;
    }
}

void
hmap_remove(hmap_t *m, const size_t index)
{
    int block_index     = index % 16;
    struct block *block = &m->data[index / 16];
    uint8_t metadata    = block->metadata[block_index];

    if (metadata == meta_root)
    { // delete right away
        block->metadata[block_index] = meta_empty;
        return;
    }

    if (metadata & meta_bits_jmp)
    { // replace with end of list
        size_t this_index        = index;
        struct block *this_block = block;
        int this_block_index     = block_index;

        for (;;)
        {
            const size_t next_index =
                (this_index + meta_jmp(this_block->metadata[this_block_index]))
                & m->num_indexes_minus_one;
            struct block *const next_block = &m->data[next_index / 16];
            const int next_block_index     = next_index % 16;
            const uint8_t next_metadata =
                next_block->metadata[next_block_index];

            if ((next_metadata & meta_bits_jmp) == 0)
            {
                this_block->metadata[this_block_index] &= meta_child;
                next_block->metadata[next_block_index] = meta_empty;
                block->key[block_index] = next_block->key[next_block_index];
                block->val[block_index] = next_block->val[next_block_index];
                return;
            }

            this_index       = next_index;
            this_block       = next_block;
            this_block_index = next_block_index;
        }
    }
    else
    { // delete and unlink from parent
        const void *key         = block->key[block_index];
        const hmap_hash_t hashf = m->hashf;
        size_t parent_index = hash_to_index(hashf ? hashf(key) : (size_t)key,
                                            m->index_unused_bits);
        struct block *parent_block;
        int parent_block_index;
        for (;;)
        {
            parent_block       = &m->data[parent_index / 16];
            parent_block_index = parent_index % 16;
            const uint8_t parent_metadata =
                parent_block->metadata[parent_block_index];
            size_t jmp_distance = meta_jmp(parent_metadata);
            size_t next_index =
                (parent_index + jmp_distance) & m->num_indexes_minus_one;
            if (next_index == index) break;
            parent_index = next_index;
        }

        parent_block->metadata[parent_block_index] &= meta_child;
        block->metadata[block_index] = meta_empty;
    }
}
