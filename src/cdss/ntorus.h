#ifndef CDSS_NTORUS_H
#define CDSS_NTORUS_H

#include <stdlib.h>
#include "types.h"
#include "alloc.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef struct ntorus ntorus_t;
typedef void (*ntorus_cb_t)(ntorus_t *, cdss_integer_t *data);

ntorus_t *ntorus_create(const unsigned int dimensions,
                        const unsigned long size[],
                        cdss_integer_t default_value,
                        const cdss_alloc_t *allocator);
static inline void ntorus_destroy(ntorus_t *n) {free(n);}
cdss_integer_t *ntorus_at(ntorus_t *, const long pos[]);
void ntorus_foreach(ntorus_t *,
                    const long low[],
                    const long high[],
                    ntorus_cb_t cb);//inclusive
void ntorus_fill(ntorus_t *,
                 const long low[],
                 const long high[],
                 cdss_integer_t data);//inclusive
void ntorus_move(ntorus_t *, const long pos[]);
void ntorus_shift(ntorus_t *, const long diff[]);
void ntorus_pos(ntorus_t *, long pos[]);
void ntorus_set_default(ntorus_t *, cdss_integer_t default_value);
void ntorus_callback_out(ntorus_t *, ntorus_cb_t func);
void ntorus_callback_in(ntorus_t *, ntorus_cb_t func);


#ifdef __cplusplus
}
#endif

#endif
