#ifndef CDSS_HASH_H
#define CDSS_HASH_H

#include <stdint.h>

uintptr_t hash_fib(uintptr_t);

// 64-bit FNV1-a
// http://www.isthe.com/chongo/tech/comp/fnv/ is a good link
uintptr_t hash_faststr(const unsigned char *s);

#endif
