#ifndef CDSS_HMAP_H
#define CDSS_HMAP_H

#include <stdint.h>
#include <stdlib.h>

#include "alloc.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct hmap hmap_t;
typedef size_t hmap_index_t;

// zero on match (so strcmp is valid)
typedef int (*hmap_cmp_t)(const void *, const void *);
typedef uintptr_t (*hmap_hash_t)(const void *);

hmap_t *     hmap_create(const unsigned long size, hmap_hash_t, hmap_cmp_t);
void         hmap_destroy(hmap_t *m);
hmap_index_t hmap_find(const hmap_t *, const void *key);
const void * hmap_at(const hmap_t *, const hmap_index_t);
int          hmap_insert(hmap_t *, const void *key, const void *val);
void         hmap_remove(hmap_t *, const hmap_index_t);
void         hmap_set_max_load(hmap_t *, double);
size_t       hmap_num_keys(const hmap_t *);

#ifdef __cplusplus
}
#endif

#endif
