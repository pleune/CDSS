#ifndef CDSS_BASETYPE_H
#define CDSS_BASETYPE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef union {
    void *        pointer;
    int           sint;
    unsigned      uint;
    long          slong;
    unsigned long ulong;
} cdss_integer_t;

#define CDSS_INT(x) (cdss_integer_t){.sint = x}
#define CDSS_UINT(x) (cdss_integer_t){.uint = x}
#define CDSS_LONG(x) (cdss_integer_t){.slong = x}
#define CDSS_ULONG(x) (cdss_integer_t){.ulong = x}
#define CDSS_PTR(x) (cdss_integer_t){.pointer = x}

#define CDSS_INT_CMP_ALL(a, b)                                                                     \
    (((a).sint == (b).sint && (a).uint == (b).uint && (a).slong == (b).slong                       \
      && (a).ulong == (b).ulong && (a).pointer == (b).pointer))

#ifdef __cplusplus
}
#endif

#endif
