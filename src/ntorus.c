#include "cdss/ntorus.h"
#include "cdss/minmax.h"
#include "cdss/modulo.h"

#include <math.h>

struct ntorus {
    unsigned int dimensions;
    ntorus_cb_t in, out;
    cdss_alloc_t allocator;
    cdss_integer_t default_value;
    cdss_integer_t data[];
};

/*
 *    struct ntorus
 * +-------------------------+
 * | size_t dimensions       |
 * | size_t size[dimensions] |
 * | size_t pos[dimensions]  | //IMPORTANT: this ASSUMES that size_t
 * | DATA                    | //has the same or higher alignment
 * +-------------------------+ //requirements of void *
 */

static inline cdss_integer_t *
size_segment(struct ntorus *n)
{
    return &n->data[0];
}

static inline cdss_integer_t *
pos_segment(struct ntorus *n)
{
    return &n->data[n->dimensions];
}

static inline cdss_integer_t *
data_segment(struct ntorus *n)
{
    return &n->data[2*n->dimensions];
}

ntorus_t *
ntorus_create(unsigned int dimensions, const unsigned long size[], cdss_integer_t default_value, const cdss_alloc_t *allocator)
{
    size_t data_len = size[0];
    size_t i;

    for(i=1; i<dimensions; i++)
        data_len *= size[i];

    struct ntorus *ret;
    size_t alloc_size =
        sizeof(struct ntorus) +
        sizeof(*ret->data) * (2*dimensions + data_len);

    if(allocator)
    {
        if(allocator->type == ALLOC_ASYM)
        {
            ret = allocator->u.asymmetric.alloc(allocator->u.asymmetric.argument,
                                                alloc_size);
        } else {
            if(allocator->u.symmetric.size >= alloc_size)
                ret = allocator->u.symmetric.alloc(allocator->u.symmetric.argument);
            else
                return 0;//symmetric allocator is not big enough...
        }
        ret->allocator = *allocator;
    } else {
        ret = malloc(alloc_size);
        ret->allocator.type = ALLOC_NONE;
    }

    ret->dimensions = dimensions;
    ret->in = ret->out = 0;
    ret->default_value = default_value;

    for(i=0; i<dimensions; i++)
        size_segment(ret)[i].ulong = size[i];

    size_t b;
    for(b=0; b<data_len; b++)
        data_segment(ret)[b] = default_value;

    return ret;
}

inline cdss_integer_t *
ntorus_at(ntorus_t *n, const long pos[])
{
    size_t index = 0;
    size_t i;
    size_t mux;

    for(i = 0, mux = 1; i<n->dimensions; i++)
    {
        index += MODULO(pos[i], size_segment(n)[i].ulong) * mux;
        mux *= size_segment(n)[i].ulong;
    }

    return data_segment(n)+index;
}

void
ntorus_foreach(ntorus_t *n, const long low[], const long high[], ntorus_cb_t cb)
{
    long stack[n->dimensions];

    size_t i;
    for(i=0; i<n->dimensions; i++)
    {
        if(low[i] > high[i])
            return;
    }

    for(i=0; i<n->dimensions; i++)
        stack[i] = low[i];

    for(;;)
    {
        cb(n, ntorus_at(n, stack));

        i = n->dimensions-1;
        while(++stack[i] > high[i])
        {
            stack[i] = low[i];
            if(i == 0) return;
            i--;
        }
    }
}

void
ntorus_fill(ntorus_t *n, const long low[], const long high[], cdss_integer_t data)
{
    long stack[n->dimensions];

    size_t i;
    for(i=0; i<n->dimensions; i++)
    {
        if(low[i] > high[i])
            return;
    }

    for(i=0; i<n->dimensions; i++)
        stack[i] = low[i];

    for(;;)
    {
        *ntorus_at(n, stack) = data;

        i = n->dimensions-1;
        while(++stack[i] > high[i])
        {
            stack[i] = low[i];
            if(i == 0) return;
            i--;
        }
    }
}

void
ntorus_move(ntorus_t *n, const long pos[])
{
    unsigned i;
    long shift[n->dimensions];

    for(i = 0; i<n->dimensions; i++)
        shift[i] = pos[i] - pos_segment(n)[i].slong;

    ntorus_shift(n, shift);
}

void
ntorus_shift(ntorus_t *n, const long diff[])
{
    unsigned i;
    int low[n->dimensions];
    int high[n->dimensions];

    for(i = 0; i<n->dimensions; i++)
    {
        low[i] = 0;
        high[i] = size_segment(n)[i].ulong;
    }

    for(i = 0; i<n->dimensions; i++)
    {
        unsigned j;
        long low_[n->dimensions];
        long high_[n->dimensions];

        int changed = MIN(diff[i], size_segment(n)[i].ulong);
        high[i] = changed;

        for(j=0; j<n->dimensions; j++)
        {
            low_[j] = pos_segment(n)[j].slong + low[j];
            high_[j] = pos_segment(n)[j].slong + high[j] - 1;
        }

        if(n->out) ntorus_foreach(n, low_, high_, n->out);
        ntorus_fill(n, low_, high_, n->default_value);
        if(n->in) ntorus_foreach(n, low_, high_, n->in);

        low[i] = changed;
        high[i] = size_segment(n)[i].ulong;
    }

    for(i = 0; i<n->dimensions; i++)
        pos_segment(n)[i].slong += diff[i];
}

void
ntorus_pos(ntorus_t *n, long pos[])
{
    unsigned i;
    for(i=0; i<n->dimensions; i++)
        pos[i] = pos_segment(n)[i].slong;
}

void
ntorus_set_default(ntorus_t *n, cdss_integer_t default_value)
{
    n->default_value = default_value;
}

void
ntorus_callback_out(ntorus_t *n, ntorus_cb_t func)
{
    n->out = func;
}

void
ntorus_callback_in(ntorus_t *n, ntorus_cb_t func)
{
    n->in = func;
}
