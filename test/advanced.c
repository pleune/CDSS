#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "cdss/mpool_grow.h"
#include "cdss/ntorus.h"
#include "cdss/voxtree.h"

#include "test.h"

static cdss_integer_t cintzero = {.uint = 0};

static long util_ntorus_cbi    = 0;
static cdss_integer_t util_ntorus_set_to = { .slong = 5};

static void
util_ntorus_cb(ntorus_t *n, cdss_integer_t *data)
{
    *data = util_ntorus_set_to;
    util_ntorus_cbi++;
}

static int
test_ntorus_basic(void)
{
    unsigned long size[3]   = {10, 10, 10};
    long move1[3]  = {4000, 19000, 400};
    long shift1[3] = {1, 1, 1};

    long it[3];
    long out1[2][10][10] = {{{9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}},

                            {{9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
                             {9, 5, 5, 5, 5, 5, 5, 5, 5, 5},
                             {9, 5, 5, 5, 5, 5, 5, 5, 5, 5},
                             {9, 5, 5, 5, 5, 5, 5, 5, 5, 5},
                             {9, 5, 5, 5, 5, 5, 5, 5, 5, 5},
                             {9, 5, 5, 5, 5, 5, 5, 5, 5, 5},
                             {9, 5, 5, 5, 5, 5, 5, 5, 5, 5},
                             {9, 5, 5, 5, 5, 5, 5, 5, 5, 5},
                             {9, 5, 5, 5, 5, 5, 5, 5, 5, 5},
                             {9, 5, 5, 5, 5, 5, 5, 5, 5, 5}}};

    ntorus_t *n = ntorus_create(3, size, CDSS_LONG(1), 0);
    ntorus_callback_in(n, &util_ntorus_cb);
    ntorus_move(n, move1);

    if (util_ntorus_cbi != 1000)
    {
        printf("%lu\n", util_ntorus_cbi);
        return EXIT_FAILURE;
    }

    for (it[0] = 0; it[0] < size[0]; it[0]++)
    for (it[1] = 0; it[1] < size[1]; it[1]++)
    for (it[2] = 0; it[2] < size[2]; it[2]++)
        if (ntorus_at(n, it)->slong != 5) return EXIT_FAILURE;

    util_ntorus_cbi    = 0;
    util_ntorus_set_to = CDSS_LONG(9);

    ntorus_shift(n, shift1);
    if (util_ntorus_cbi != 271) return EXIT_FAILURE;


    for (it[0] = 0; it[0] < size[0]; it[0]++)
    for (it[1] = 0; it[1] < size[1]; it[1]++)
    for (it[2] = 0; it[2] < 2      ; it[2]++)
        if (ntorus_at(n, it)->slong != out1[it[2]][it[1]][it[0]]) return EXIT_FAILURE;

    ntorus_destroy(n);

    return EXIT_SUCCESS;
}

static int
test_voxtree_basic(void)
{
    voxtree_t *tree = voxtree_create(3, 5, 0, cintzero); // 32x32x32

    // void *i = (void *)100;
    cdss_integer_t i = {.uint = 100};

    unsigned long pos[3] = {10, 20, 10};
    voxtree_set(tree, pos, i);
    i = voxtree_get(tree, pos);

    if (i.uint != 100) return EXIT_FAILURE;
    voxtree_set(tree, pos, cintzero);
    if (voxtree_count_nodes(tree) > 1) return EXIT_FAILURE;
    voxtree_destroy(tree);

    return EXIT_SUCCESS;
}

static long noise_cb_int;
static void
noise_cb(int isleaf, const voxtree_region_t *region)
{
    noise_cb_int++;
}

static int
test_voxtree_noise(void)
{
    voxtree_t *  tree             = voxtree_create(3, 5, 0, cintzero); // 32x32x32
    unsigned int data[32][32][32] = {{{0}}};
    srand(time(0));
    noise_cb_int = 0;

    long         i;
    int          x, y, z;
    unsigned int t = 0;
    for (i = 0; i < 1000000; i++)
    {
        x = rand() % 32;
        y = rand() % 32;
        z = rand() % 32;
        t = t + 1;

        data[x][y][z] = t;

        unsigned long pos[3] = {x, y, z};
        voxtree_set(tree, pos, (cdss_integer_t){.uint = t});
    }

    voxtree_iterate_nodes(tree, &noise_cb, 0);

    if (voxtree_count_nodes(tree) != noise_cb_int) return EXIT_FAILURE;

    for (x = 0; x < 32; x++)
        for (y = 0; y < 32; y++)
            for (z = 0; z < 32; z++)
            {
                unsigned long pos[3] = {x, y, z};

                t = voxtree_get(tree, pos).uint;
                if (t != data[x][y][z]) return EXIT_FAILURE;
            }

    t = 0;

    for (x = 0; x < 32; x++)
        for (y = 0; y < 32; y++)
            for (z = 0; z < 32; z++)
            {
                unsigned long pos[3] = {x, y, z};
                voxtree_set(tree, pos, (cdss_integer_t){.uint = t});
            }

    if (voxtree_count_nodes(tree) > 1) return EXIT_FAILURE;
    voxtree_destroy(tree);
    return EXIT_SUCCESS;
}

static int
test_voxtree_high_dim(void)
{
    voxtree_t *  tree                   = voxtree_create(6, 3, 0, cintzero); // 8x8x8x8x8x8
    unsigned int data[8][8][8][8][8][8] = {{{{{{0}}}}}};

    long          i;
    unsigned long it[6];
    unsigned int  t = 0;

    srand(time(0));

    for (i = 0; i < 1000000; i++)
    {
        int j;
        for (j = 0; j < 6; j++) it[j] = rand() % 8;
        t = t + 1;

        data[it[0]][it[1]][it[2]][it[3]][it[4]][it[5]] = t;
        voxtree_set(tree, it, (cdss_integer_t){.uint = t});
    }

    for (it[0] = 0; it[0] < 8; it[0]++)
    for (it[1] = 0; it[1] < 8; it[1]++)
    for (it[2] = 0; it[2] < 8; it[2]++)
    for (it[3] = 0; it[3] < 8; it[3]++)
    for (it[4] = 0; it[4] < 8; it[4]++)
    for (it[5] = 0; it[5] < 8; it[5]++)
    {
        t = voxtree_get(tree, it).uint;
        if (t != data[it[0]][it[1]][it[2]][it[3]][it[4]][it[5]]) return EXIT_FAILURE;
    }

    t = 0;

    for (it[0] = 0; it[0] < 8; it[0]++)
    for (it[1] = 0; it[1] < 8; it[1]++)
    for (it[2] = 0; it[2] < 8; it[2]++)
    for (it[3] = 0; it[3] < 8; it[3]++)
    for (it[4] = 0; it[4] < 8; it[4]++)
    for (it[5] = 0; it[5] < 8; it[5]++) { voxtree_set(tree, it, (cdss_integer_t){.uint = t}); }

    if (voxtree_count_nodes(tree) > 1) return EXIT_FAILURE;
    voxtree_destroy(tree);
    return EXIT_SUCCESS;
}

static int
test_voxtree_mpool(void)
{
    mpool_gr_t * pool      = mpool_gr_create(4096 * 16, voxtree_get_alloc_size(3), 8);
    cdss_alloc_t allocator = mpool_gr_allocator(pool);
    voxtree_t *  tree      = voxtree_create(3, 5, &allocator, cintzero); // 32x32x32

    unsigned int data[32][32][32] = {{{0}}};

    srand(time(0));

    long         i;
    int          x, y, z;
    unsigned int t = 0;
    for (i = 0; i < 1000000; i++)
    {
        x = rand() % 32;
        y = rand() % 32;
        z = rand() % 32;
        t = t + 1;

        data[x][y][z] = t;

        unsigned long pos[3] = {x, y, z};
        voxtree_set(tree, pos, (cdss_integer_t){.uint = t});
    }

    for (x = 0; x < 32; x++)
    for (y = 0; y < 32; y++)
    for (z = 0; z < 32; z++)
    {
        unsigned long pos[3] = {x, y, z};

        t                    = voxtree_get(tree, pos).uint;
        if (t != data[x][y][z]) return EXIT_FAILURE;
    }

    t = 0;

    for (x = 0; x < 32; x++)
    for (y = 0; y < 32; y++)
    for (z = 0; z < 32; z++)
    {
        unsigned long pos[3] = {x, y, z};
        voxtree_set(tree, pos, (cdss_integer_t){.uint = t});
    }

    if (voxtree_count_nodes(tree) > 1) return EXIT_FAILURE;

    voxtree_destroy(tree);
    mpool_gr_destroy(pool);

    return EXIT_SUCCESS;
}

static int
test_voxtree_sphere(void)
{
    mpool_gr_t * pool      = mpool_gr_create(1024 * 64, voxtree_get_alloc_size(3), 8);
    cdss_alloc_t allocator = mpool_gr_allocator(pool);
    voxtree_t *  tree      = voxtree_create(3, 10, &allocator, cintzero); // 1024x1024x1024

    unsigned long pos[3];

#define x pos[0]
#define y pos[1]
#define z pos[2]

    for (x = 0; x < 1024; x++)
    for (y = 0; y < 50; y++)
    for (z = 0; z < 50; z++)
        if (sqrt(x * x + y * y + z * z) < 50) voxtree_set(tree, pos, (cdss_integer_t){.uint = 1});

    noise_cb_int = 0;
    voxtree_iterate_nodes(tree, &noise_cb, 0);
    if (voxtree_count_nodes(tree) != noise_cb_int) return EXIT_FAILURE;

    voxtree_destroy(tree);
    mpool_gr_destroy(pool);

#undef x
#undef y
#undef z

    return EXIT_SUCCESS;
}

TESTING_START
ADD_TEST(test_ntorus_basic, "N-Torus")
ADD_TEST(test_voxtree_basic, "Voxtree - basic")
ADD_TEST(test_voxtree_noise, "Voxtree - noise")
ADD_TEST(test_voxtree_high_dim, "Voxtree - high dimentional")
ADD_TEST(test_voxtree_mpool, "Voxtree - mpool integration")
ADD_TEST(test_voxtree_sphere, "Voxtree - sphere")
TESTING_END
