#include <time.h>
#include "cdss/tpool.h"
#include "test.h"

static void
util_tpool_simple(void *v)
{
    static struct timespec t;
    t.tv_sec = 0;
    t.tv_nsec = 10000000;//10ms
    nanosleep(&t, 0);
}

static int
test_tpool_basic(void)
{
    tpool_t *pool = tpool_create(2);

    size_t i;
    for(i=0; i<10; i++)//100ms
        tpool_add(pool, &util_tpool_simple, 0, 0);

    tpool_flush(pool);
    tpool_destroy(pool);

    return EXIT_SUCCESS;
}

static int
test_tpool_max_threads(void)
{
    tpool_t *pool = tpool_create(256);

    size_t i;
    for(i=0; i<256*10; i++)//100ms
        tpool_add(pool, &util_tpool_simple, 0, 0);

    tpool_flush(pool);
    tpool_destroy(pool);

    return EXIT_SUCCESS;
}

TESTING_START
ADD_TEST(test_tpool_basic, "Tpool - basic")
ADD_TEST(test_tpool_max_threads, "Tpool - max thread")
TESTING_END
