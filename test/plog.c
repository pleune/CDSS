#include "cdss/plog.h"
#include <unistd.h>

#include "test.h"

static int
test_basic(void)
{
    size_t i;
    for(i=0; i<100; i++)
        plog(L_DEBUG, "DEBUG CRAP NOT SHOWN BY DEFAULT %i", i);

    for(i=0; i<100; i++)
        plog(L_INFO, "SOME FAIRLY USELESS INFO %i", i);

    for(i=0; i<100; i++)
        plog(L_WARN, "SOME WARNINGS %i", i);

    for(i=0; i<100; i++)
        plog(L_ERROR, "SOME ERRORS %i", i);

    plog_flush();

    return EXIT_SUCCESS;
}

static int
test_fatal(void)
{
    plog(L_FATAL, "FATAL ERRORS SHOULD DIE");
    plog_flush();

    return EXIT_SUCCESS;
}

TESTING_START
ADD_TEST(test_basic, "Basic")
ADD_TEST_ADV(test_fatal, "Fatal", 1, 1, 0)
TESTING_END
