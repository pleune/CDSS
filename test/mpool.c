#include <time.h>
#include <stdlib.h>

#include "cdss/mpool_static.h"
#include "cdss/mpool_grow.h"
#include "cdss/mpool_dynamic.h"

#include "test.h"

static int
test_st_basic(void)
{
    mpool_st_t *pool = mpool_st_create(4096*8, sizeof(long long), 8);
    long long **pointers = malloc(10000*sizeof(long long *));
    size_t i;

    //exaust pool
    for(i=0; i<10000; i++)
        pointers[i] = mpool_st_alloc(pool);

    //exausted pool shouldnt alloc
    if(mpool_st_alloc(pool))
        return EXIT_FAILURE;

    //write zero to make sure we dont lead pointers
    for(i=0; i<10000; i++)
        if(pointers[i]) pointers[i][0] = 0;

    //make sure the writes stuck
    for(i=0; i<10000; i++)
        if(pointers[i])
            if(pointers[i][0] != 0)
                return EXIT_FAILURE;

    //free everything
    for(i=0; i<10000; i++)
    {
        if(pointers[i])
            mpool_st_free(pool, pointers[i]);
        pointers[i] = 0;
    }

    //jump around like crazy allocing and freeing, making sure values
    //stick and are not corrupted
    srand(time(0));
    for(i=0; i<1000000; i++)
    {
        size_t j = rand()%10000;

        if(pointers[j])
        {
            if(pointers[j][0] != (int)j) return EXIT_FAILURE;
            mpool_st_free(pool, pointers[j]);
            pointers[j] = 0;
        } else {
            pointers[j] = mpool_st_alloc(pool);
            if(pointers[j]) pointers[j][0] = j;
        }
    }

    //free everything
    for(i=0; i<10000; i++)
    {
        if(pointers[i])
        {
            if(pointers[i][0] != (int)i) return EXIT_FAILURE;
            mpool_st_free(pool, pointers[i]);
        }
    }

    mpool_st_destroy(pool);
    free(pointers);

    return EXIT_SUCCESS;
}

static int
test_gr_basic(void)
{
    mpool_gr_t *pool = mpool_gr_create(1024, sizeof(int), 8);
    int **pointers = malloc(10000*sizeof(int *));
    size_t i;

    //linear test
    for(i=0; i<10000; i++)
        pointers[i] = mpool_gr_alloc(pool);

    for(i=0; i<10000; i++)
        pointers[i][0] = i;

    for(i=0; i<10000; i++)
        if(pointers[i][0] != (int)i) return EXIT_FAILURE;

    for(i=0; i<10000; i++)
        mpool_gr_free(pool, pointers[i]);


    //errotic test to scramble free list
    srand(time(0));

    for(i=0; i<10000; i++)
        pointers[i] = 0;

    for(i=0; i<1000000; i++)
    {
        size_t j = rand()%10000;

        if(pointers[j])
        {
            if(pointers[j][0] != (int)j) return EXIT_FAILURE;
            mpool_gr_free(pool, pointers[j]);
            pointers[j] = 0;
        } else {
            pointers[j] = mpool_gr_alloc(pool);
            pointers[j][0] = j;
        }
    }

    for(i=0; i<10000; i++)
    {
        if(pointers[i])
        {
            if(pointers[i][0] != (int)i) return EXIT_FAILURE;
            mpool_gr_free(pool, pointers[i]);
        }
    }

    mpool_gr_destroy(pool);
    free(pointers);

    return EXIT_SUCCESS;
}

static int
test_dy_basic(void)
{
    mpool_dy_t *pool = mpool_dy_create(1024*16, sizeof(int), 8);
    int **pointers = malloc(10000*sizeof(int *));
    size_t i;

    //linear test
    for(i=0; i<10000; i++)
        pointers[i] = mpool_dy_alloc(pool);

    for(i=0; i<10000; i++)
        pointers[i][0] = i;

    for(i=0; i<10000; i++)
        if(pointers[i][0] != (int)i) return EXIT_FAILURE;

    for(i=0; i<10000; i++)
        mpool_dy_free(pool, pointers[i]);

    srand(time(0));

    for(i=0; i<10000; i++)
        pointers[i] = 0;

    for(i=0; i<1000000; i++)
    {
        size_t j = rand()%10000;

        if(pointers[j])
        {
            if(pointers[j][0] != (int)j) return EXIT_FAILURE;
            mpool_dy_free(pool, pointers[j]);
            pointers[j] = 0;
        } else {
            pointers[j] = mpool_dy_alloc(pool);
            pointers[j][0] = j;
        }
    }

    for(i=0; i<10000; i++)
    {
        if(pointers[i])
        {
            if(pointers[i][0] != (int)i) return EXIT_FAILURE;
            mpool_dy_free(pool, pointers[i]);
        }
    }

    if(mpool_dy_blocks(pool) != 1)
        return EXIT_FAILURE;

    mpool_dy_destroy(pool);
    free(pointers);

    return EXIT_SUCCESS;
}

TESTING_START
ADD_TEST(test_st_basic, "Static")
ADD_TEST(test_gr_basic, "Grow")
ADD_TEST(test_dy_basic, "Dynamic")
TESTING_END
