#include <string.h>
#include <stdio.h>
#include "cdss/stack.h"
#include "cdss/ringbuff.h"
#include "cdss/hmap.h"
#include "test.h"

static int
test_stack_basic(void)
{
    stack_t *stack = stack_create(sizeof(int), 2, 2.0);
    if(stack_objects_get_num(stack) != 0)
        return EXIT_FAILURE;

    int i;
    int j;

    for(i=0; i<10000; i++)
        stack_push(stack, &i);

    if(stack_objects_get_num(stack) != 10000)
        return EXIT_FAILURE;

    for(i=0; i<10000; i++)
    {
        stack_pop(stack, &j);
        if(j != 9999-i)
            return EXIT_FAILURE;
    }

    stack_destroy(stack);

    return EXIT_SUCCESS;
}

static int
test_stack_advanced(void)
{
    stack_t *stack = stack_create(sizeof(int), 100, 1.5);

    int i;

    stack_ensure_size(stack, 10000);

    for(i=0; i<10000; i++)
        *(int *)(stack_element_ref(stack, i)) = i;

    stack_advance(stack, 10000);

    for(i=0; i<10000; i++)
    {
        int j;
        stack_pop(stack, &j);
        if(j != 9999-(int)i)
            return EXIT_FAILURE;
    }

    int *data = stack_transform_dataptr(stack);
    stack = stack_create(sizeof(int), 100, 2.0);
    stack_trim(stack);

    stack_push_mult(stack, data, 10000);

    for(i=0; i<10000; i++)
    {
        int j;
        stack_pop(stack, &j);
        if(j != 9999-(int)i)
            return EXIT_FAILURE;
    }

    for(i=0; i<10000; i++)
        stack_push(stack, &i);

    for(i=0; i<9999; i++)
    {
        stack_element_replace_from_end(stack, 0);
        if(*(int *)stack_element_ref(stack, 0) != 9999 - i)
            return EXIT_FAILURE;
    }

    if(stack_objects_get_num(stack) != 1)
        return EXIT_FAILURE;

    if(*(int *)stack_element_ref(stack, 0) != 1)
        return EXIT_FAILURE;

    for(i=0; i<9999; i++)
        stack_push(stack, &i);

    stack_resize(stack, 100);

    if(stack_objects_get_num(stack) != 100)
        return EXIT_FAILURE;

    free(data);
    stack_destroy(stack);

    return EXIT_SUCCESS;
}

static int
test_ringbuff_basic(void)
{
    ringbuff_t *buff = ringbuff_create(100);
    const char *origional = "abcdefghijklmnopqrstuvwxyz";
    size_t i;

    //test to make sure data is read/written intact
    for(i=0; i<100000; i++)
    {
        char returned[27];
        ringbuff_put(buff, origional, 27);
        ringbuff_put(buff, origional, 27);

        ringbuff_remove(buff, returned, 27);
        if(strncmp(returned, origional, 27) != 0)
            return EXIT_FAILURE;

        ringbuff_remove(buff, returned, 27);
        if(strncmp(returned, origional, 27) != 0)
            return EXIT_FAILURE;
    }

    if (!ringbuff_empty(buff)) return EXIT_FAILURE;

    //test overflow stability
    for(int i=0; i<100; i++)
        ringbuff_put(buff, origional, 27);

    for(int i=0; i<100; i++)
        ringbuff_remove(buff, 0, 27);

    ringbuff_destroy(buff);

    return EXIT_SUCCESS;
}

static uintptr_t
hmap_hash_one(const void *p)
{
    return 1;
}

static uintptr_t
hmap_hash_mixed(const void *p)
{
    if ((intptr_t)p < 55)
        return 1;
    else
        return (uint64_t)p;
}

static int
test_hmap_simple()
{
    hmap_t *h = hmap_create(10, 0, 0);

    hmap_insert(h, (void *)1, (void *)1);
    hmap_insert(h, (void *)3, (void *)3);
    hmap_insert(h, (void *)5, (void *)5);

    if(hmap_at(h, hmap_find(h, (void *)1)) != (void *)1) return EXIT_FAILURE;
    if(hmap_at(h, hmap_find(h, (void *)3)) != (void *)3) return EXIT_FAILURE;
    if(hmap_at(h, hmap_find(h, (void *)5)) != (void *)5) return EXIT_FAILURE;

    hmap_destroy(h);
    return EXIT_SUCCESS;
}

static int
test_hmap_bad_hash()
{
    hmap_t *h = hmap_create(10, &hmap_hash_one, 0);

    hmap_insert(h, (void *)1, (void *)1);
    hmap_insert(h, (void *)3, (void *)3);
    hmap_insert(h, (void *)5, (void *)5);

    if(hmap_at(h, hmap_find(h, (void *)1)) != (void *)1) return EXIT_FAILURE;
    if(hmap_at(h, hmap_find(h, (void *)3)) != (void *)3) return EXIT_FAILURE;
    if(hmap_at(h, hmap_find(h, (void *)5)) != (void *)5) return EXIT_FAILURE;

    hmap_destroy(h);
    return EXIT_SUCCESS;
}

static int
test_hmap_strings()
{
    hmap_t *h = hmap_create(10, &hmap_hash_one, (hmap_cmp_t)&strcmp);

    hmap_insert(h, "KEY1", (void *)1);
    hmap_insert(h, "Key2", (void *)3);
    hmap_insert(h, "keY3", (void *)5);

    if(hmap_at(h, hmap_find(h, "KEY1")) != (void *)1) return EXIT_FAILURE;
    if(hmap_at(h, hmap_find(h, "Key2")) != (void *)3) return EXIT_FAILURE;
    if(hmap_at(h, hmap_find(h, "keY3")) != (void *)5) return EXIT_FAILURE;

    hmap_destroy(h);
    return EXIT_SUCCESS;
}

static int
test_hmap_fill()
{
    size_t num_tests = 1000000;

    void **keys = malloc(sizeof(void *) * num_tests);
    for(size_t i=0; i<num_tests; i++) keys[i] = (void *)i;

    for(size_t i=0; i<num_tests; i++)
    {
        void **a = &keys[rand()%num_tests];
        void **b = &keys[rand()%num_tests];
        void *tmp = *a;
        *a = *b;
        *b = tmp;
    }

    hmap_t *h = hmap_create(16, 0, 0);

    for(size_t i=0; i<num_tests; i++)
        hmap_insert(h, keys[i], (void *)(intptr_t)i);

    for(size_t i=0; i<num_tests; i++)
    {
        const void *val = hmap_at(h, hmap_find(h, keys[i]));
        if(val != (void *)(uintptr_t)i) return EXIT_FAILURE;
    }

    uint64_t num_keys = hmap_num_keys(h);
    if(num_keys != num_tests)
        return EXIT_FAILURE;

    hmap_destroy(h);
    free(keys);

    return EXIT_SUCCESS;
}

static int
test_hmap_erase()
{
    size_t num_tests = 1000000;

    void **keys = malloc(sizeof(void *) * num_tests);
    for(size_t i=0; i<num_tests; i++) keys[i] = (void *)(i+1);

    for(size_t i=0; i<num_tests; i++)
    {
        void **a = &keys[rand()%num_tests];
        void **b = &keys[rand()%num_tests];
        void *tmp = *a;
        *a = *b;
        *b = tmp;
    }

    hmap_t *h = hmap_create(16, 0, 0);

    for(size_t i=0; i<num_tests; i++)
        hmap_insert(h, keys[i], (void *)(intptr_t)i);

    for(size_t i=0; i<num_tests/2; i++)
    {
        size_t key = rand()%num_tests;
        if(keys[key] != 0)
        {
            hmap_remove(h, hmap_find(h, keys[key]));
            keys[key] = 0;
        }
    }

    for(size_t i=0; i<num_tests; i++)
    {
        hmap_index_t index = hmap_find(h, keys[i]);

        if(keys[i] == 0)
        {
            if (index != (hmap_index_t)-1) return EXIT_FAILURE;
        }
        else
        {
            const void *val = hmap_at(h, index);
            if(val != (void *)(uintptr_t)i) return EXIT_FAILURE;
        }
    }

    uint64_t num_keys = hmap_num_keys(h);
    if(num_keys != num_tests)
        return EXIT_FAILURE;

    hmap_destroy(h);
    free(keys);

    return EXIT_SUCCESS;
}

TESTING_START
ADD_TEST(test_stack_basic, "Stack - Basic")
ADD_TEST(test_stack_advanced, "Stack - Adv.")
ADD_TEST(test_ringbuff_basic, "Ringbuff")
ADD_TEST(test_hmap_simple, "Hash Map - no collisions")
ADD_TEST(test_hmap_bad_hash, "Hash Map - collisions")
ADD_TEST(test_hmap_strings, "Hash Map - strings")
ADD_TEST(test_hmap_fill, "Hash Map - fill")
ADD_TEST(test_hmap_erase, "Hash Map - erase")
TESTING_END
