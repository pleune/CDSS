#ifndef CDSS_TEST_H
#define CDSS_TEST_H

#include <stdlib.h>

typedef int (*work_t)(void);

typedef struct {
    work_t func;
    const char *name;
    int seconds;
    int expected_bad_return;
    int expected_signal;
} test_t;

int run_all(int argc, char **argv, test_t *test_list, int num_tests);

#define TESTING_START int main(int argc, char **argv) { test_t t[] = {
#define ADD_TEST_ADV(func, name, sec, success, signal) {&func, name, sec, success, signal},
#define ADD_TEST(func, name) {&func, name, 1, 0, 0},
#define TESTING_END }; return run_all(argc, argv, t, sizeof(t)/sizeof(*t)); }

#endif
